<p align="center"><a href="http://fabacademy.org/2020/labs/stjude/00_learning_resources.html"><img src="images/00_intro_header.png" alt="header with logos"></p>

# Inkscape interface

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i> Getting to know Inkscape

---

## Installing

To install Inkscape, go [here](https://inkscape.org/release/inkscape-1.0/) and download the correct file for your computer. Once downloaded, generally all that you need to do is double-click on the file and the installation willl begin. As said before, it is more convenient to install it in english.

## Interface

If you already installed Inkscape, look for it and open it. Before anything else, let's make sure we have the same tool distribution. For this, go to **`View`** menu and select **`Default`**.

<p align="center"><img width="400" src="images/01_intro_07_inkscape_view_menu.jpg" alt="activate default view"></p>

You should then see something like this:

<p align="center"><img src="images/01_intro_08_inkscape_ui.png" alt="interface parts"></p>

1. Menu
2. Commands bar
3. Context menu (changes according to the tool you select)
4. Toolbox

## Common drawing tools and operations

The most common operations are located in the **`Path`** menu:

<p align="center"><img width="400" src="images/01_intro_09_path_menu.png" alt="activate default view"></p>

and the most common tools are these:

<p align="center"><img width="400" src="images/01_intro_10_inkscape_tools.png" alt="activate default view"></p>
