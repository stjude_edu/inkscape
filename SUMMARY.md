# Summary

* [1. Inkscape and vectors](01_0_intro.md)
	* [1.1 Uses](01_1_uses.md)
	* [1.2 Inkscape](01_2_interface.md)
* [2. Basic shapes](02_0_basic_shapes.md)
* [3. Basic operations](03_0_basic_operations.md)
* [4. Vectorizing](04_0_vectorizing.md)
* [5. Exporting](05_0_exporting.md)
	* [5.1 svg](05_1_svg.md)
	* [5.2 png](05_2_png.md)
	* [5.3 dxf](05_3_dxf.md)

