<p align="center"><a href="http://fabacademy.org/2020/labs/stjude/00_learning_resources.html"><img src="images/00_intro_header.png" alt="header with logos"></p>

# St. Jude FabLab tutorials

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i> Collection of tutorials about different tools for the class and the fablab.
Aimed for teachers and students.

---

<p align="center">
<img src="images/00_intro_logos.png" alt="vector and raster comparison">
</p>

##Hello, welcome to this collection of tutorials!

You can find more tutorials [here](http://fabacademy.org/2020/labs/stjude/00_learning_resources.html).

Created by: [FabLab St. Jude](http://fabacademy.org/2020/labs/stjude/index.html).

Contact: jmiranda@stjude.ed.cr
