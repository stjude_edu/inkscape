<p align="center"><a href="http://fabacademy.org/2020/labs/stjude/00_learning_resources.html"><img src="images/00_intro_header.png" alt="header with logos"></p>

# Uses

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i> Uses of vector and raster graphics

---

## Imaging

Raster images are used generally for photography, vectors are mostly used for logos, icons and simpler images like clipart.

You can convert images from vector to raster and vice versa:

<p align="center"><img src="images/01_intro_05_photo_raster_vector.jpg" alt="zoom on vector and raster comparison"></p>

<p align="center">image taken from: <a href="https://commons.wikimedia.org/wiki/Main_Page" target="_blank"> commons.wikimedia.org</a> and vectorized with Inkscape</p>

but in general, photos should remain as raster, and logos and other simple images should remain as vectors.

## Digital fabrication

Another use for vector graphics is in digital fabrication. The machines interpret vectors in order to perform the fabrication process.

You can for example take a raster image, convert it to vectors and have a clear view of the outline. This is what a machine will interpret to cut, engrave or print the design.

<p align="center"><img src="images/01_intro_06_logo_conversion.png" alt="conversion from raster to vector outlines"></p>
