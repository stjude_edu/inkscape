<p align="center"><a href="http://fabacademy.org/2020/labs/stjude/00_learning_resources.html"><img src="images/00_intro_header.png" alt="header with logos"></p>

# Inkscape

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i> Official site: [https://inkscape.org](https://inkscape.org/en/)

---
<p align="center"><img src="images/01_intro_01_logo_inkscape.png" alt="vector and raster comparison"></p>

Inkscape is a vector graphics software. Available for windows, mac and linux (not for ChromeOS). It is free to download and use.

You can go to [this site](https://inkscape.org/release/inkscape-1.0/) to download the software. Download and install the appropriate file for your computer (there is currently no version available for Chromebook).

Even with multiple languages options for installing Inkscape, it is preferable to use the **english version** because the majority of help articles and tutorials will be in english.

## What is vector graphics?

There are two types of 2D computer graphics: raster and vector.

Raster graphics examples are pictures you take with a camera. They have a dot matrix structure. Each dot is called a pixel. When you zoom in on a raster image, you can see these pixels. Low resolution images appear pixelated, and shapes are not well defined. High resolution images are well defined, but still are made up of small pixels.

Vector images are defined mathematically. Dots are conected by lines and/or curves to form shapes. So, when you zoom in on a vector image, the shapes are recalculated and the definition appears always high.

<p align="center">
<img src="images/01_intro_02_raster_vector_tree.png" alt="vector and raster comparison">
</p>
<p align="center">
image taken from: <a href="http://www.freesvgclipart.com/" target="_blank"> freesvgclipart.com</a>
</p>

These two images look the same, but when you zoom in, you can see the difference between them:

<p align="center">
<img src="images/01_intro_03_zoom_raster_vector.jpg" alt="zoom on vector and raster comparison">
</p>

Another area:

<p align="center">
<img src="images/01_intro_04_zoom_raster_vector.jpg" alt="zoom on vector and raster comparison">
</p>
